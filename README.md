<img alt="" style="border-width:0" src="https://www.dropbox.com/s/sxe7ozdw18m7kli/banner.png?raw=1">
## Curso de Introducción a Física de Partículas, Nuclear, Aceleradores y Detectores
### Escuela José Antonio Balseiro - Nuevas Tendencias en Investigación en Física Médica
### Octubre 2016 - Centro Atómico Bariloche e Instituto Balseiro

El curso consta de cuatro clases de aproximadamente tres horas y media cada una, cubriendo los siguientes tópicos a un nivel introductorio:

* Clase 1: Cinemática y Dinámica Relativista. Interacciones fundamentales. Física de partículas. Generalidades sobre el Modelo Estándar
* Clase 2: Cargas conservadas y simetrías de la Naturaleza. El núcleo atómico: energía de ligadura, defecto y exceso de masa. Estabilidad nuclear. Momento angular, espín y paridad. El modelo de capas. Radiactividad.
* Clase 3: Decaimientos radiactivos. Actividad. Unidades. Tipos de decaimiento y descripción de los principales. Ejemplos de núcleos radiactivos de uso médico e industrial y sus principales líneas de emisión. Física de Aceleradores: principios de funcionamiento y tipos. Sección eficaz y luminosidad. 
* Clase 4: Interacción de la radiación con la materia: poder de frenado, pico de Bragg, rango, longitud de interacción, energía crítica, coeficientes de atenuación lineal y másico. Características de detectores de uso en física de partículas: fototubos, fotomultiplicadores de silicio, Cherenkov y centelleo.

En las clases se incluyen algunos ejercicios y problemas y bibliografía recomendada. Se incluye tambien el código stopping.py (python v2.7) para calcular el pico de Bragg y el rango de partículas cargadas en medios materiales.  

#### (C) 2016 - Hernán Asorey ([@asoreyh](https://twitter.com/asoreyh/))

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Este trabajo se distribuye en forma gratuita bajo la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licencia Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a>.
